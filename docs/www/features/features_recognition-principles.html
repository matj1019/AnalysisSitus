<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: feature recognition principles</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->

  <!--
     Quick navigation script. Use a div with "toc-panel" class having a
     nested div with "toc" id to place the navigation panel.
    -->
  <script src="../js/jquery-3.5.1.min.js"></script>
  <script src="../js/toc.js"></script>

 </head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../features.html">features</a>/recognition principles
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='http://quaoar.su/blog/page/analysis-situs'>Download</a></td>
  <td class="header-menu"><a href="../features.html">Features</a></td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</table></tr></td>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="toc-feat-rec">CAD feature recognition in nutshell</h1>

<div class="toc-panel"><div id="toc"></div></div>

<p>
  Compared to B-rep faces, features are somewhat higher-level entities that capture the correspondence between design information and manufacturing activities. For example, while CAD data expresses the shape, features communicate how a specific artifact might be manufactured or assembled <a href="../references.html#regli-1995">[Regli, 1995]</a>. One may see a feature as an "energy port" as proposed by <a href="../references.html#voelcker-1989">[Shapiro and Voelcker, 1995]</a>.
</p>

<p>
  This section uncovers some technical details of the feature recognition framework availble in Analysis Situs. Read the <a href="./features_feature-recognition-framework.html">feature recognition overview</a> chapter for the basics.
</p>

<h2 id="toc-feat-rec-methods">Feature recognition approaches</h2>

<p>
  We use two approaches to feature recognition in Analysis Situs and derived applications:
</p>

<ol>
  <li>Rule-based.</li>
  <li><a href="./features_find-isomorphous-faces.html">Isomorphism</a>.</li>
</ol>

<p class="note">
  We do not employ any machine learning techniques, although we do provide means to prepare data for ML, such as <a href="./features_voxelization.html">voxelization</a>. You can find more information on the AI approaches in feature recognition, for example, in <a href="../references.html#ghadai-2018">[Ghadai et al, 2018]</a>.
</p>

<p>
  The rule-based approach is pretty fast. It can also deal with some feature interactions. At the same time, this method is not highly extendable. Still, this method is somewhat standard as it is probably the most straightforward way to get the result. The essence of the approach is to iterate all B-rep faces while attempting to compose them into groups based on some ad-hoc heuristics. Some heuristics are listed in our brief conference paper <a href="../references.html#malyshev-rule-based-2017">[Malyshev et al, 2017]</a>.
</p>

<p class="note">
  There is a bit of ambiguity in referring to some feature recognition approach as "graph-based." For example, <a href="./features_find-isomorphous-faces.html">finding isomorphous faces</a> is completely abstract and graph-based as it searches for a pattern subgraph in the entire model <a href="./features_aag.html">AAG</a>. At the same time, the rule-based approach is also "graph-based" as it matches just the same AAG, though in different ways (i.e., by iteration).
</p>

<h2 id="toc-feat-rec-rule-based">Recognizer/Rule architecture</h2>

<p>
  Historically first, this approach remains our favorite. If implemented correctly, it gives the right balance between recognition accuracy and efficiency. All rules are programmed in C++, and that's arguably the downside of the approach, because any customization would require programming and recompilation. At the same time, the method is highly maintainable as it is entirely deterministic and straightforward. The following notions are used throughout the rule-based recognition methods:
</p>

<table align="center" style="border: 1px solid rgb(100, 100, 100);" cellpadding="5" cellspacing="0" width="100%">
<tr>
  <td align="center" class="table-content-header">
    Recognizer
  </td>
  <td class="table-content-block">
    <p>
      The Recognizer is a C++ class, which is the entry point to the recognition routine. The Recognizer accepts a CAD model (or its <a href="./features_aag.html">AAG</a>) and returns the set of faces (and their indices) corresponding to the detected features. There's always an option to store the corresponding attributes in AAG to make the extracted features persistent.
    </p>
    <p>
      The Recognizer contains the main loop of iterating over the B-rep faces. It may also search for the seed faces that look promising to start recognition from. In fact, it can just iterate over all faces of the model and let the Rules (see below) decide whether they can start recognition from the current face or not. In that sense, the Recognizer handles a recognition "cursor" by moving it over the AAG and letting the Rules do some real job.
    </p>
    <p>
      The Recognizer manages the Rules. It may apply several Rules if necessary.
    </p>
  </td>
</tr>
<tr>
  <td align="center" class="table-content-header">
    Rule
  </td>
  <td class="table-content-block">
    <p>
      For the given position of AAG iterator (a "cursor"), a Rule performs <i>local</i> recognition by applying a certain set of geometric and topological heuristics. The Rule is allowed to visit any faces of the AAG, so it can be seen as a nested loop (or loops) for the main recognition loop. All visited nodes are usually memorized to avoid checking them twice in the main loop.
    </p>
    <div class="note">
      One of the fundamental heuristics is the <a href="./features_check-dih-angles.html">dihedral angle</a> between feature faces.
    </div>
    <p>
      A typical Rule employs the following stages:
    </p>
    <ol>
      <li><i>Get ready:</i> take the current face and mark it as "visited."</li>
      <li><i>Apply heuristics:</i> attempt to recognize a feature starting from the current face inclusively (the iterated faces are marked as "visited" down the road).</li>
      <li><i>Compose the result</i> in the case of success, and return all "visited" faces as a result.</li>
    </ol>
    <p>
      The Recognizer checks the Rule's status at each iteration. If a Rule fails, the Recognizer cancels the "visited" faces returned by a Rule. If a Rule succeeds, the Recognizer makes sure to skip the visited faces, i.e., to avoid passing them as new seeds at subsequent iterations.
    </p>
  </td>
</tr>
</table>

<h2 id="toc-feat-workflow">Recognition workflow</h2>

<p>
  The feature recognition tools available in Analysis Situs might be combined freely. They all work on the AAG constructed out of the initial shape. You may choose to build up a new AAG each time you run a recognizer or, alternatively, reuse the same AAG and collect all recognized features as its attributes. Whatever option you prefer, the recognizers will give you a <a href="./features_recognition-anatomy-of-feature.html">series of indices</a> to address the detected feature faces uniquely.
</p>

<p>
  Imagine you detect all cylindrical holes in the model using the rule-based approach. Let's also assume that the applied rule does not differentiate between different types of holes, i.e., you get through, blind, countersunk, and counterbored holes all in one result set. To distinguish between various hole types, you can apply isomorphism matching. That would be a good idea since the rule-based recognizers have narrowed down the entire search space, and you have as many connected components out of the initial AAG as many features were detected. Finding isomorphous faces is a computationally expensive operation, and partitioning the initial graph will speed up the process drastically (see <a href="../references.html#slyadnev-graphs-2020">[Slyadnev et al, 2020]</a> for more details).
</p>

<h2 id="toc-feat-examples">Examples of rule-based recognizers</h2>

<p>
  The following feature types were recognized with the rule-based Recognizer/Rule architecture using the Analysis Situs framework:
</p>

<ol>
  <li>Drilled holes.</li>
  <li>Cylindrical shafts.</li>
  <li>Arbitrary cavities.</li>
  <li>Modeled threads.</li>
</ol>

<p>
  The Recognizer/Rule (RR) architecture is nothing but a good practice of recognizing well-defined local features. Other feature types, such as blends, chamfers, milling contours, turned faces, etc. might require their own dedicated architecture.
</p>

<h1 id="toc-feat-isomorphism">Isomorphism</h1>

<p>
  Finding isomorphous faces is a straightforward way for feature matching. The entire process can be broken down to the following steps:
</p>

<ol>
  <li class="text-block">Decompose the initial AAG onto as many connected component subgraphs as you can. This would allow for running the isomorphism algorithm independently and on the reduced search space. One best practice is to use the rule-based recognition for the broad-phase decomposition, and then apply series of isomorphism matches over the decomposed subgraphs.</li>
  <li class="text-block">Prepare the dictionary of features you want to recognize. By "dictionary" we mean a domain-specific storage of feature descriptors that are going to be matched over the CAD part in question. This can be anything, starting from a text file to a remote database holding all feature patterns relevant to your application, machnining process or organization.</li>
</ol>

<p>
  The following image illustrates a "stress test" CAD model for the feature recognizer. The model contains three types of holes: countersunk, counterbored, and simple cylindrical holes, in many possible variations.
</p>

<div align="center"><img src="../imgs/situ_fr_many-holes.png"/></div>

<p>
  Recognizing a hole with a rule-based approach may end up with a bunch of hole features, separated from each other. Running the isomorphism matching on each component would allow for extracting such properties as bore's diameter, sink angle, etc. The job of isomorphism is to match the pattern's faces that have some semantics attached to the faces with unknown semantics.
</p>

<div align="center"><img src="../imgs/situ_fr_many-holes-recognized.png"/></div>

<p>
  The base class for all rule-based recognizers, which is <span class="code-inline">asiAlgo_Recognizer</span> provides a virtual method named <span class="code-inline">matchConnectedComponent()</span> that is supposed to be overriden by the subclasses. This method accepts a single connected component (e.g., a generic hole) and lets the derived logic to extract all missing properties, such as bore or sink holes' faces using the isomorphism technique.
</p>

<h1 id="toc-feat-limitations">Limitations</h1>

<p>
  There are some limitations in the recognition methods that employ geometric and topological heuristics:
</p>

<ol>
  <li>The input model should be a <a href="./features_check-shape.html">valid</a> solid.</li>
  <li>The CAD faces should be maximized.</li>
  <li>The canonical faces should be represented with the analytical surfaces (e.g., if a face is visually planar, it needs to be declared as a plane in the underlying B-rep).</li>
</ol>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    Copyright &copy; Analysis&nbsp;Situs 2015-present &nbsp; | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://analysis-situs.medium.com/" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

</body>
</html>
