<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: attributed adjacency graph</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->

 </head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../features.html">features</a>/AAG
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='http://quaoar.su/blog/page/analysis-situs'>Download</a></td>
  <td class="header-menu"><a href="../features.html">Features</a></td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</table></tr></td>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1>AAG overview</h1>

<blockquote class="quote">"AAG = Amazing Adjacency Graph" (its grateful users).</blockquote>

<p>
Attributed adjacency graph (AAG) is the auxiliary data structure which is often used for feature recognition.
The concept of AAG is well described in the paper by <a href="../references.html#joshi-chang-1988">[Joshi and Chang, 1988]</a>.
Unlike the face adjacency graph (FAG) presented by <a href="../references.html#ansaldi-et-al-1985">[Ansaldi et al, 1985]</a>,
our implementation of the graph permits only single arcs between the nodes <a href="../references.html#slyadnev-graphs-2020">[Slyadnev et al, 2020]</a>. In our AAG implementation, an arc expresses
a neighborhood relation rather than its actual realization by means of boundary edges.
The following figure gives an example of AAG built for a synthetic model.
</p>

<div align="center"><img src="../imgs/aag_illustration.png"/></div>

<h2>Motivation</h2>

<p>AAG is a convenient shape descriptor. It serves the following purposes:</p>

<ol>
  <li class="text-block">AAG contains information on face neighborhood explicitly. The neighborhood relations are expressed as graph arcs.</li>
  <li class="text-block">Edge vexity (concave or convex property) is stored as arc attribute (hence the graph is called "attributed").
      Edge vexity is a fundamental geometric heuristic, especially useful for feature recognition.</li>
  <li class="text-block">AAG enables graph formalism for operating with CAD models. E.g., using AAG, it is straightforward to find
      features as subgraphs by the given search pattern.</li>
  <li class="text-block">AAG serves as a natural cache for exploration maps: it stores the internal indexation of faces, edges, and vertices. Hence
      it is not necessary to recompute indexation in algorithms.</li>
  <li class="text-block">Since a B-Rep model in OpenCascade does not contain any attributes, the question on how to bind supplementary data with
      geometric entities often arises. AAG gives a convenient way to assign custom attributes with its nodes and arcs. For example,
      it is possible to store information on the recognized features right in the AAG. Therefore, the feature recognition process may consist
      in enriching the graph with more and more information which you were able to extract from the dumb model.</li>
</ol>

<div align="center"><img src="../imgs/aag_example.png"/></div>

<h2>Adjacency relationship</h2>

<p>
  Adjacency relationships between faces is expressed by graph links (arcs) connecting the corresponding graph nodes. A link exists if there is at least one common edge between a couple of faces. If the faces share more than one edge, there is still exactly one link in the graph, although the corresponding edges are cached in one of the arc attributes. Therefore, AAG captures the edge-based connectivity of a model.
</p>

<p>
  It might sometimes be useful to add extra arcs to the graph to represent vertex-adjacency relationships. A vertex-adjacency relationship is a situation when two faces have a common vertex and no common edges. For example, this can be a situation at a vertex blend.
</p>

<div align="center"><img src="../imgs/aag-vertex-adjacency_01.png"/></div>

<p>
  AAG does not capture such vertex-based adjacency relations, but the corresponding arcs can be added on demand. The following picture illustrates an AAG built with edge-adjacency arcs only:
</p>

<div align="center"><img src="../imgs/aag-vertex-adjacency_02.png"/></div>

<p>
  Adding the vertex-adjacency relations makes the graph more complex as the following image shows (gray arcs). Notice also that the convexity attributes are not computed as there is no edge to assess the dihedral angle for:
</p>

<div align="center"><img src="../imgs/aag-vertex-adjacency_03.png"/></div>

<h1>Dumping to JSON</h1>

<p>
Analysis Situs allows dumping the attributed adjacency graph in JSON format for subsequent use in external feature recognizers. Here is
the example of the serialized AAG corresponding to a simple box solid:
</p>

<div class="code"><pre>{
  "nodes": {
    "1": {
      "surface": "plane"
    },
    "2": {
      "surface": "plane"
    },
    "3": {
      "surface": "plane"
    },
    "4": {
      "surface": "plane"
    },
    "5": {
      "surface": "plane"
    },
    "6": {
      "surface": "plane"
    }
  },
  "arcs": [
    ["1", "3", "convex"],
    ["1", "4", "convex"],
    ["1", "5", "convex"],
    ["1", "6", "convex"],
    ["2", "3", "convex"],
    ["2", "4", "convex"],
    ["2", "5", "convex"],
    ["2", "6", "convex"],
    ["3", "5", "convex"],
    ["3", "6", "convex"],
    ["4", "5", "convex"],
    ["4", "6", "convex"]
  ]
}</pre></div>

<p>
There are two blocks in the JSON output: the "nodes" and the "arcs". Each node has the corresponding 1-based ID. The IDs of the nodes
are referenced in the "arcs" block. Be aware that the attributed adjacency graph is not oriented, so the order of node IDs has no sense
in the link specification. If there are any attributes associated with AAG nodes, these attributes are also dumped.
</p>

<p>
To dump AAG for the active part to a JSON-file, use the following Tcl command:
</p>

<div class="code">
  dump-aag-json &lt;filename&gt;
</div>

<h1>Q & A</h1>

<h4>Why is there a dihedral angle attribute on cylindrical faces?</h4>

<p>This attribute is induced by the "seam" edges of periodic faces, i.e., the edges corresponding to angles 0 and 2*PI on a cylinder (it also applies to spheres, cones, surfaces of revolution, periodic splines, etc).</p>

<p><i>Concave angle:</i></p>
<img src="../imgs/situ_aag_qa_01.png"/>

<p><i>Convex angle:</i></p>
<img src="../imgs/situ_aag_qa_02.png"/>

<p>The dihedral angles of seam edges are available as <i>nodal</i> AAG attributes of the corresponding faces.</p>

<h4>How do I know if an edge is an inner one on a face?</h4>

<p>This information is not encoded in AAG directly. If you are looking for faces having some inner loops, you may want to try <span class="code-inline">recognize-base-faces</span> Tcl command. If you really need to enumerate the inner edges, consider doing that in C++.</p>

<div class="code" style="font-size: smaller"><pre>
void ComputeInnerEdges(const TopoDS_Face&          face,
                       TopTools_IndexedMapOfShape& edges)
{
  TopoDS_Wire outerWire = BRepTools::OuterWire(face);
  
  // Explore wires.
  for ( TopExp_Explorer explW(face, TopAbs_WIRE); explW.More(); explW.Next() )
  {
    const TopoDS_Wire& candWire = TopoDS::Wire( explW.Current() );
    //
    if ( candWire.IsPartner(outerWire) )
      continue;
  
    // Explore edges.
    for ( TopExp_Explorer explE(candWire, TopAbs_EDGE); explE.More(); explE.Next() )
    {
      const TopoDS_Edge& E = TopoDS::Edge( explE.Current() );
  
      // Add to the result.
      edges.Add(E);
    }
  }
}
</pre></div>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    Copyright &copy; Analysis&nbsp;Situs 2015-present &nbsp; | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://analysis-situs.medium.com/" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

</body>
</html>
